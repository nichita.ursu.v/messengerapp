import { Component, Output, EventEmitter } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'chat-input',
  templateUrl: './chat-input.component.html',
  styles: [ '.chat-input { padding: 10px; }' ]
})
export class ChatInputComponent {

  @Output() sent: EventEmitter<string> = new EventEmitter();

  text: FormControl;

  constructor(private _fb: FormBuilder) {
    this.text = _fb.control([ null ]);
  }

  sendMessage(): void {
    const { value } = this.text;

    this.text.setValue(null);
    if (value !== ' ' && value !== '' && value ) this.sent.emit(value);
  }
}