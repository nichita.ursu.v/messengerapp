import { Component, Input, Attribute } from '@angular/core';

@Component({
  selector: 'avatar-img',
  templateUrl: './avatar.component.html',
  styleUrls: [ './avatar.component.scss']
})
export class AvatarComponent {

  src: string;
  alt: string;

  @Input() set contact(contact: any) {
    this.src = contact.avatar;
    this.alt = `${contact.firstName[0]}${contact.lastName[0]}`.toUpperCase();
  }

  constructor(
    @Attribute('wide') public wide: boolean,
  ) {}
}