import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  AvatarComponent,
  ChatInputComponent,
} from './components';

@NgModule({
  declarations: [
    AvatarComponent,
    ChatInputComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    AvatarComponent,
    ChatInputComponent,
  ]
})
export class SharedModule {}