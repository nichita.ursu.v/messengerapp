import { trigger, transition, state, style, animate } from '@angular/animations';

export const CHAT_INACTIVE = 'chatInactive';
export const CHAT_PLACEHOLDER = 'chatPlaceholder';
export const CHAT_ACTIVE = 'chatActive';
export type ChatAnimationsState = 'chatInactive' | 'chatPlaceholder' | 'chatActive';

const transitions = [
  transition('chatInactive => *', animate('80ms ease-in')),
  transition('chatActive => *', animate('80ms ease-in')),
  transition('* => chatInactive', animate('80ms ease-out')),
  transition('* => chatActive', animate('80ms ease-out'))
];

export const animations = [
  trigger('contacts', [
    state(CHAT_INACTIVE, style({
      width: '95%'
    })),
    state(CHAT_PLACEHOLDER, style({
      width: '70%'
    })),
    state(CHAT_ACTIVE, style({
      width: '25%'
    })),
    ...transitions
  ]),

  trigger('chat', [
    state(CHAT_INACTIVE, style({
      width: '5%'
    })),
    state(CHAT_PLACEHOLDER, style({
      width: '30%'
    })),
    state(CHAT_ACTIVE, style({
      width: '75%'
    })),
    ...transitions
  ])
]