import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IContact, IChatData, IMessage } from '../defs';
import { MessagesService } from '../services';
import {
  animations,
  ChatAnimationsState,

  CHAT_ACTIVE,
  CHAT_INACTIVE,
  CHAT_PLACEHOLDER,
} from './container.animations';

@Component({
  templateUrl: './container.component.html',
  styleUrls: [ './container.component.scss' ],
  animations
})
export class MessengerContainerComponent implements OnInit {

  contacts: Array<IContact>;
  chatData: IChatData;
  animationsState: ChatAnimationsState = CHAT_INACTIVE;

  constructor(
    private _route: ActivatedRoute,
    private _messages: MessagesService,
  ) {
    this.contacts = _route.snapshot.data.contacts;
  }

  ngOnInit(): void { this.revindProgress(); }

  updateState(contact: IContact): void {
    const messages: Array<IMessage> = this._messages.messagesByConversationId(contact.id);

    this.chatData = { contact, messages };
    this.saveProgress(contact.id);
    this.animationsState = messages.length > 0 ? CHAT_ACTIVE : CHAT_PLACEHOLDER;
  }

  private saveProgress(id: number): void {
    localStorage.setItem('chat-opened', id.toString());
  }

  private revindProgress(): void {
    const progress = localStorage.getItem('chat-opened');

    if (progress) {
      const contact = this.contacts.filter(contact => contact.id === +progress)[0];
      this.updateState(contact);
    }
  }
}