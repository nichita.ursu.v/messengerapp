import { Component, Input } from '@angular/core';
import { ExpandType } from '../../defs';

@Component({
  selector: 'contacts-list',
  templateUrl: './contacts.component.html',
  styleUrls: [ './contacts.component.scss']
})
export class ContactsComponent {}