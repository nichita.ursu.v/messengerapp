import { Component, Input } from '@angular/core';

import { IContact } from '../../../defs';

@Component({
  selector: 'contact-element',
  templateUrl: './contact-element.component.html',
  styleUrls: [ './contact-element.component.scss']
})
export class ContactElementComponent {

  @Input() contact: IContact;

  get avatarAlt(): string {
    const { firstName, lastName } = this.contact;

    return `${firstName[0].toUpperCase()}${lastName[0].toUpperCase()}`;
  }

  get contactLocation(): string {
    const { country, city } = this.contact;

    return `${country}, ${city}`;
  }
}