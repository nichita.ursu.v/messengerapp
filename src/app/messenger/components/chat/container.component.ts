import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IMessage, IChatData, IContact } from '../../defs';

import { MessagesService } from '../../services';

@Component({
  selector: 'chat',
  templateUrl: './container.component.html',
  styleUrls: [ './container.component.scss']
})
export class ChatContainerComponent {

  @Input() data: IChatData;
  @Output() stateChanged: EventEmitter<IContact> = new EventEmitter();

  constructor(
    private _messages: MessagesService,
  ) {}

  sentMessage(text: string): void {
    const { id } = this.data.contact;
    const message: IMessage = {
      text,
      conversationWith: id,
      contactId: 'HOST'
    };

    this._messages.sentMessage(message);
    this.stateChanged.emit(this.data.contact);
  }

  get isHistoryEmpty (): boolean {
    return this.data.messages.length === 0;
  }
}