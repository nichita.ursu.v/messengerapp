import { Component, Input, ViewChild, AfterViewChecked } from '@angular/core';
import { IChatData } from '../../../defs';

@Component({
  selector: 'chat-history',
  templateUrl: './chat-history.component.html',
  styleUrls: [ './chat-history.component.scss']
})
export class ChatHistoryComponent implements AfterViewChecked {

  @ViewChild('scrollbar') content;
  @Input() data: IChatData;

  ngAfterViewChecked(): void {
    this.scrollToBottom();
  }

  private scrollToBottom(): void {
    this.content.directiveRef.scrollToBottom();
  }
}