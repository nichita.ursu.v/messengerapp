import { Component, Input } from '@angular/core';
import { IMessage } from '../../../../defs';

@Component({
  selector: 'message-item',
  templateUrl: './message-item.component.html',
  styleUrls: [ './message-item.component.scss']
})
export class MessageItemComponent {
  @Input() message: IMessage;
}