import { Component, Input } from '@angular/core';
import { IContact } from '../../../defs';

@Component({
  selector: 'chat-placeholder',
  templateUrl: './chat-placeholder.component.html',
  styleUrls: [ './chat-placeholder.component.scss']
})
export class ChatPlaceholderComponent {

  @Input() contact: IContact;

  get contactLocation(): string {
    return `${this.contact.country}, ${this.contact.city}`;
  }
}