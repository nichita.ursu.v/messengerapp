import { Injectable } from '@angular/core';

import { IMessage } from '../defs';
import { uniqueId } from 'lodash';

@Injectable()
export class MessagesService {

  private messages: Array<IMessage> = [
    { conversationWith: 1, contactId: 1, text: 'Привет, сегодня буду в 8.' },
    { conversationWith: 1, contactId: 1, text: 'Ключей нет!' },
    { conversationWith: 3, contactId: 3, text: 'Нет времени.' },
    { conversationWith: 6, contactId: 6, text: 'Привет' },
    { conversationWith: 6, contactId: 6, text: 'Нужно скинуть деньги на др Жени. По 20$' },
    { conversationWith: 6, contactId: 'HOST', text: 'Извини, на этой неделе болею сильно(' },
    { conversationWith: 6, contactId: 'HOST', text: 'Попробую передать как-то.' },
    { conversationWith: 12, contactId: 12, text: 'TESTTESTESTESTTEST' },
    { conversationWith: 20, contactId: 'HOST', text: 'Lorem ipsum....' },
    { conversationWith: 21, contactId: 21, text: 'sit dolar amet.' },
  ];

  messagesByConversationId(id: number): Array<IMessage> {
    return this.messages.filter(msg => msg.conversationWith === id);
  }

  sentMessage(message: IMessage): IMessage {
    const newMessage = { id: uniqueId(), ...message };
    this.messages.push(newMessage);

    return newMessage;
  }
}