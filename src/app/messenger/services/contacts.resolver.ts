import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { ContactsService } from './contacts.service';
import { IContact } from '../defs';

@Injectable()
export class ContactsResolver implements Resolve<Array<IContact>> {

  constructor(private _contacts: ContactsService) {}

  async resolve() {
    return await this._contacts.list().toPromise();
  }
}