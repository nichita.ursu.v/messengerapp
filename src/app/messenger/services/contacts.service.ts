import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IContact } from '../defs';
import { Observable } from 'rxjs';

@Injectable()
export class ContactsService {

  constructor(
    private _http: HttpClient,
  ) {}

  list(): Observable<Array<IContact>> {
    return this._http.get<Array<IContact>>('/assets/mock-data/contacts.json');
  }
}