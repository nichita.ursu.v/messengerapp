export interface IContact {
  id: number
  lastName: string;
  fullName: string;
  firstName: string;
  country: string;
  city: string;
  age: number;
  about: string;
  avatar?: string;
}