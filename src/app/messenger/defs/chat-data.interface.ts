import { IContact, IMessage } from './';

export interface IChatData {
  contact: IContact;
  messages: Array<IMessage>
};