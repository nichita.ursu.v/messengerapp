export interface IMessage {
  conversationWith: number;
  contactId: string | number;
  text: string;
  id?: string;
}