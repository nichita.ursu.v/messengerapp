import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SharedModule } from '../shared';

import {
  ContactsService,
  ContactsResolver,
  MessagesService,
} from './services';

import { MessengerRoutingModule } from './messenger-routing.module';
import {
  MessengerContainerComponent,

  ContactsComponent,
  ContactElementComponent,

  ChatContainerComponent,
  ChatPlaceholderComponent,
  ChatHistoryComponent,
  MessageItemComponent,
} from './components';

@NgModule({
  declarations: [
    MessengerContainerComponent,

    ContactsComponent,
    ContactElementComponent,

    ChatContainerComponent,
    ChatPlaceholderComponent,
    ChatHistoryComponent,
    MessageItemComponent,
  ],
  imports: [
    SharedModule,
    HttpClientModule,
    MessengerRoutingModule,
    PerfectScrollbarModule,
  ],
  providers: [
    ContactsService,
    ContactsResolver,
    MessagesService,
  ]
})
export class MessengerModule {}