import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MessengerContainerComponent } from './components';
import { ContactsResolver } from './services';

const messengerRoutes: Routes = [
  { path: '', component: MessengerContainerComponent, resolve: { contacts: ContactsResolver } }
];

export const MessengerRoutingModule: ModuleWithProviders = RouterModule.forChild( messengerRoutes );