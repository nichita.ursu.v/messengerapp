import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: '', loadChildren: './messenger/messenger.module#MessengerModule' },
  { path: '**', redirectTo: '' }
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot( appRoutes );